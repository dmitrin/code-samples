<?php

namespace App\Services;

use App\Config;
use App\Helpers\{Iblock, Form};
use App\Services\NotificationService\NotificationTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Web\HttpClient;
use CForm, CIBlockElement, CEventLog, CSubscription, Exception;


/**
 * Класс по работе с платформой автоматизации email-маркетинга - Mailganer
 */
class MailganerService
{
    protected $user;

    protected $action;
    protected $data;
    protected $options;

    protected $apiKey;
    protected $apiSource;

    const API_URL = 'https://mailganer.com/api/v2';
    const API_MAX_USERS = 50;
    const FORM_PROGRAM = 'PROGRAMM_REGISTRATION_FORM';

    const FIELD_SUBSCRIBE = 'podpisalsya_na_rassyilku';
    const FIELD_REGISTRATION = 'registrirovalsya_na_sajte';
    const FIELD_REFUSE_EVENTS = 'otkazalsya_ot_uvedomlenij_o_meropriyatiyah';
    const FIELD_REFUSE_PROGRAMS = 'otkazalsya_ot_uvedomlenij_o_programmah';
    const FIELD_REFUSE_MATERIALS = 'otkazalsya_ot_uvedomlenij_o_materialah';
    const FIELD_REGISTRATION_EVENT = 'registrirovalsya_na_meropriyatiya';
    const FIELD_APPLICATION = 'podaval_zayavku_na_programmukurs';
    const FIELD_PAYMENT = 'pokupal_bilet';

    /**
     * Конструктор класса
     *
     * @param $action
     * @param $data
     * @param $options
     *
     * @return void
     */
    private function __construct($action, $data = [], $options = [])
    {
        try {
            $this->user = $GLOBALS['USER'];

            if (!Loader::includeModule('iblock')) {
                throw new Exception('Не установлен модуль iblock');
            }

            if (empty($action)) {
                throw new Exception('Неверно передан action');
            }

            $this->apiKey = Config::getMailganerKey();
            $this->apiSource = Config::getMailganerSource();

            $this->action = $action;
            $this->data = $data;
            $this->options = $options;

            $this->process();
        } catch (Exception $exception) {
            $this->log($exception->getMessage());
        }
    }

    /**
     * Определение actions класса
     *
     * @return void
     */
    protected function process()
    {
        switch ($this->action) {
            case 'addSubscriber':
                $this->addSubscriber();
                break;
            case 'addRegisterUser':
                $this->addRegisterUser();
                break;
            case 'updateEntity':
                $this->updateEntity();
                break;
            case 'synchronizeUsers':
                $this->synchronizeUsers();
                break;
            case 'synchronizeSubscribes':
                $this->synchronizeSubscribes();
                break;
            default:
                $this->log('Неизвестный action: ' . $this->action);
                break;
        }
    }

    /**
     * Формирование экземпляра MailganerService
     *
     * @param $action
     * @param $data
     * @param $options
     *
     * @return MailganerService
     */
    public static function getInstance($action, $data = [], $options = [])
    {
        return new self($action, $data, $options);
    }

    /**
     * Формирование массива соответствий полей сайта с полями Mailganer
     *
     * @return array
     */
    public static function getMatches()
    {
        return [
            'NAME' => 'name',
            'LAST_NAME' => 'surname',
            'SECOND_NAME' => 'patronymic',
            'PERSONAL_BIRTHDAY' => 'birthday',
            'UF_COUNTRY' => 'country',
            'PERSONAL_CITY' => 'city',
            'PERSONAL_MOBILE' => 'phone',
        ];
    }

    /**
     * Обработчик при подписке пользователя
     *
     * @return void
     */
    private function addSubscriber()
    {
        if (empty($this->data['email'])) {
            $this->log('Неверно передан E-mail');
            return;
        }

        $this->data['user_vars'][self::FIELD_SUBSCRIBE] = true;
        $this->processRequest('/emails/');
    }

    /**
     * Обработчик при регистрации пользователя
     *
     * @return void
     */
    private function addRegisterUser()
    {
        if (empty($this->data['email'])) {
            $this->log('Неверно передан E-mail');
            return;
        }

        $this->data['user_vars'][self::FIELD_REGISTRATION] = true;
        $this->processRequest('/emails/');
    }

    /**
     * Обработчик обновления сущности 'Подписчик'
     *
     * @return void
     */
    private function updateEntity()
    {
        if (empty($this->data['email'])) {
            $this->log('Неверно передан E-mail');
            return;
        }

        $this->processRequest('/emails/');
    }

    /**
     * Синхронизация всех пользователей сайта
     *
     * @return void
     */
    private function synchronizeUsers()
    {
        $users = [];

        $matches = $this->getMatches();
        $usersNotifications = $this->getUsersNotifications();
        $usersEvents = $this->getUsersEvents();
        $usersApplications = $this->getUsersApplications();
        $usersPayments = $this->getUsersPayments();

        $usersResource = $this->user::GetList(
            $by = 'ID',
            $order = 'ASC',
            [],
            [
                'FIELDS' => [
                    'ID', 'NAME', 'EMAIL', 'SECOND_NAME', 'LAST_NAME', 'PERSONAL_MOBILE',
                    'PERSONAL_CITY', 'PERSONAL_GENDER', 'PERSONAL_BIRTHDAY',
                ],
            ]
        );
        while ($user = $usersResource->Fetch()) {
            if (!empty($this->options['startId'])) {
                if ($user['ID'] < $this->options['startId']) {
                    continue;
                }
            }
            if (!empty($this->options['endId'])) {
                if ($user['ID'] > $this->options['endId']) {
                    continue;
                }
            }

            $userInfo = [
                'email' => $user['EMAIL'],
                'user_vars' => [
                    self::FIELD_REGISTRATION => true
                ]
            ];

            foreach ($matches as $fieldSite => $fieldMailganer) {
                if (!empty($user[$fieldSite])) {
                    $userInfo[$fieldMailganer] = trim($user[$fieldSite]);
                }
            }

            if (!empty($user['PERSONAL_GENDER'])) {
                $userInfo['gender'] = $user['PERSONAL_GENDER'] === 'F' ? 2 : 1;
            }

            // Установка пользовательских переменных Mailganer
            if (!empty($usersNotifications[$user['ID']])) {
                if ($usersNotifications[$user['ID']]['notification-event'] === false) {
                    $userInfo['user_vars'][self::FIELD_REFUSE_EVENTS] = true;
                }
                if ($usersNotifications[$user['ID']]['notification-program'] === false) {
                    $userInfo['user_vars'][self::FIELD_REFUSE_PROGRAMS] = true;
                }
                if ($usersNotifications[$user['ID']]['notification-material'] === false) {
                    $userInfo['user_vars'][self::FIELD_REFUSE_MATERIALS] = true;
                }
            }

            if (in_array($user['ID'], $usersEvents)) {
                $userInfo['user_vars'][self::FIELD_REGISTRATION_EVENT] = true;
            }
            if (in_array($user['EMAIL'], $usersApplications)) {
                $userInfo['user_vars'][self::FIELD_APPLICATION] = true;
            }
            if (in_array($user['ID'], $usersPayments)) {
                $userInfo['user_vars'][self::FIELD_PAYMENT] = true;
            }

            $users[] = $userInfo;
        }

        // Из-за ограничения API разбиваем данные на несколько запросов и делаем ожидание между ними
        for ($i = 0; $i < count($users); $i += self::API_MAX_USERS) {
            $currentUsers = array_slice($users, $i, self::API_MAX_USERS);
            $this->data = $currentUsers;
            $this->processRequest('/emails/', true);
            sleep(1);
        }
    }

    /**
     * Синхронизация всех подписчиков сайта
     *
     * @return void
     */
    private function synchronizeSubscribes()
    {
        $subscribers = [];

        if (!Loader::includeModule('subscribe')) {
            $this->log('Не установлен модуль subscribe');
            return;
        }

        $subscribersResource = CSubscription::GetList(
            [],
            [
                'ACTIVE' => 'Y'
            ]
        );
        while ($subscriber = $subscribersResource->Fetch()) {
            $subscribers[] = [
                'email' => $subscriber['EMAIL'],
                'user_vars' => [
                    self::FIELD_SUBSCRIBE => true
                ]
            ];
        }

        // Из-за ограничения API разбиваем данные на несколько запросов и делаем ожидание между ними
        for ($i = 0; $i < count($subscribers); $i += self::API_MAX_USERS) {
            $currentSubscribes = array_slice($subscribers, $i, self::API_MAX_USERS);
            $this->data = $currentSubscribes;
            $this->processRequest('/emails/', true);
            sleep(1);
        }
    }

    /**
     * Отправка запроса в Mailganer
     *
     * @param $url
     * @param $isMultiply
     *
     * @return void
     */
    private function processRequest($url, $isMultiply = false)
    {
        $apiUrl = self::API_URL . $url;
        $additionalParams = [
            'not_doi' => true,
            'source' => $this->apiSource,
        ];

        if ($isMultiply) {
            for ($i = 0; $i < count($this->data); $i++) {
                $this->data[$i] = array_merge($this->data[$i], $additionalParams);
            }
            $params = $this->data;
        } else {
            $params = array_merge($this->data, $additionalParams);
        }

        $httpClient = new HttpClient();
        $httpClient->setHeader('Authorization', 'CodeRequest ' . $this->apiKey);
        $httpClient->setHeader('Content-Type', 'application/json', true);

        $response = $httpClient->post($apiUrl, json_encode($params));
        $result = json_decode($response, true);

        if (!empty($result['error'])) {
            $errorText = '';

            foreach ($result['error'] as $errorType) {
                if (empty($errorType)) {
                    continue;
                }

                foreach ($errorType as $keyError => $valueError) {
                    $errorText .= $keyError . ': ' . current($valueError) . '<br>';
                }
            }

            if (!empty($errorText)) {
                $this->log($errorText);
            }
        }
    }

    /**
     * Формирование списка уведомлений пользователей
     *
     * @return array
     */
    private function getUsersNotifications()
    {
        $usersNotifications = [];

        $notifications = NotificationTable::getAll();
        foreach ($notifications as $notify) {
            if (!empty($notify['CATEGORY']['XML_ID'])) {
                $usersNotifications[$notify['UF_USER_ID']][$notify['CATEGORY']['XML_ID']] = $notify['OPTION']['XML_ID'] === 'always';
            }
        }

        return $usersNotifications;
    }

    /**
     * Формирование списка регистраций на мероприятия
     *
     * @return array
     */
    private function getUsersEvents()
    {
        $events = [];

        $eventsFilter = [
            'IBLOCK_ID' => [
                Iblock::getIblockIdByCode('events_registrations'),
                Iblock::getIblockIdByCode('events_registrations_en')
            ],
            'ACTIVE' => 'Y',
        ];
        $eventsSelect = [
            'ID', 'IBLOCK_ID', 'PROPERTY_USER'
        ];
        $eventsResult = CIBlockElement::GetList([], $eventsFilter, false, false, $eventsSelect);
        while ($registration = $eventsResult->Fetch()) {
            $events[] = $registration['PROPERTY_USER_VALUE'];
        }
        $events = array_unique($events);

        return $events;
    }

    /**
     * Формирование списка заявок пользователей на программы/курсы
     *
     * @return array
     */
    private function getUsersApplications()
    {
        $emails = [];

        if (!Loader::includeModule('form')) {
            return $emails;
        }

        $columns = [];
        $answers = [];
        $answersComplete = [];

        $idForm = Form::getByCode(self::FORM_PROGRAM);
        CForm::GetResultAnswerArray($idForm, $columns, $answers, $answersComplete);
        foreach ($answersComplete as $answer) {
            $emails[] = current($answer['email'])['USER_TEXT'];
        }
        $emails = array_unique($emails);

        return $emails;
    }

    /**
     * Формирование списка оплат пользователей
     *
     * @return array
     */
    private function getUsersPayments()
    {
        $payments = [];

        $paymentsFilter = [
            'IBLOCK_ID' => Iblock::getIblockIdByCode('payment_sber'),
            'ACTIVE' => 'Y',
        ];
        $paymentsSelect = [
            'ID', 'IBLOCK_ID', 'PROPERTY_USER_ID'
        ];
        $paymentsResult = CIBlockElement::GetList([], $paymentsFilter, false, false, $paymentsSelect);
        while ($payment = $paymentsResult->Fetch()) {
            $payments[] = $payment['PROPERTY_USER_ID_VALUE'];
        }
        $payments = array_unique($payments);

        return $payments;
    }


    /**
     * Подготовка пользовательских полей уведомлений
     *
     * @param $idUser
     * @param $emailUser
     *
     * @return void
     */
    public static function prepareNotify($idUser, $emailUser)
    {
        $userInfo = [];
        $userInfo['email'] = $emailUser;

        $currentNotify = NotificationTable::findByUserId($idUser);
        foreach ($currentNotify as $notify) {
            if ($notify['CATEGORY']['XML_ID'] === 'notification-event') {
                $userInfo['user_vars'][self::FIELD_REFUSE_EVENTS] = $notify['OPTION']['XML_ID'] !== 'always';
            }
            if ($notify['CATEGORY']['XML_ID'] === 'notification-program') {
                $userInfo['user_vars'][self::FIELD_REFUSE_PROGRAMS] = $notify['OPTION']['XML_ID'] !== 'always';
            }
            if ($notify['CATEGORY']['XML_ID'] === 'notification-material') {
                $userInfo['user_vars'][self::FIELD_REFUSE_MATERIALS] = $notify['OPTION']['XML_ID'] !== 'always';
            }
        }

        self::getInstance('updateEntity', $userInfo);
    }

    /**
     * Подготовка стандартных полей для обновления в Mailganer
     *
     * @param $emailUser
     * @param $fields
     *
     * @return void
     */
    public static function prepareUpdateUser($emailUser, $fields)
    {
        $userInfo = [];
        $userInfo['email'] = $emailUser;
        $matches = self::getMatches();

        foreach ($fields as $key => $field) {
            if (!empty($matches[$key])) {
                $userInfo[$matches[$key]] = trim($field);
            }
            if ($key === 'PERSONAL_GENDER') {
                $userInfo['gender'] = $field === 'F' ? 2 : 1;
            }
        }

        self::getInstance('updateEntity', $userInfo);
    }

    /**
     * Подготовка поля 'Подавал заявку на программу/курс'
     *
     * @param $emailUser
     *
     * @return void
     */
    public static function prepareApplication($emailUser)
    {
        self::prepareUserField($emailUser, self::FIELD_APPLICATION);
    }

    /**
     * Подготовка поля 'Регистрировался на мероприятия'
     *
     * @param $emailUser
     *
     * @return void
     */
    public static function prepareRegistrationEvent($emailUser)
    {
        self::prepareUserField($emailUser, self::FIELD_REGISTRATION_EVENT);
    }

    /**
     * Подготовка поля 'Покупал билет'
     *
     * @param $emailUser
     *
     * @return void
     */
    public static function preparePayment($emailUser)
    {
        self::prepareUserField($emailUser, self::FIELD_PAYMENT);
    }

    /**
     * Подготовка пользовательского поля
     *
     * @param $emailUser
     * @param $field
     *
     * @return void
     */
    public static function prepareUserField($emailUser, $field)
    {
        $userInfo = [
            'email' => $emailUser,
            'user_vars' => [
                $field => true
            ]
        ];

        self::getInstance('updateEntity', $userInfo);
    }

    /**
     * Логирование интеграции
     *
     * @param $description
     *
     * @return void
     */
    public function log($description)
    {
        CEventLog::Add([
            'SEVERITY' => 'DEBUG',
            'AUDIT_TYPE_ID' => 'MAILGANER',
            'MODULE_ID' => 'main',
            'DESCRIPTION' => $description,
        ]);
    }
}
