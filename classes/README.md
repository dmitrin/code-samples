# Класс интеграции с Mailganer

<strong>Mailganer</strong> - платформа автоматизации email-маркетинга. В различных местах сайта идет обновление базы подписчиков в Mailganer.

## Примеры использования

### Регистрация

В файле обработки регистрации после успешной регистрации пользователя используем следующий код:

```php
// Отправляем информацию о пользователе в Mailganer
$userInfo = [
    'email' => $email,
    'name' => $name,
    'surname' => $lastName,
    'phone' => $data['PERSONAL_MOBILE'],
    'city' => $data['PERSONAL_CITY'],
];
if (!empty($data['SECOND_NAME'])) {
    $userInfo['patronymic'] = $data['SECOND_NAME'];
}
MailganerService::getInstance('addRegisterUser', $userInfo);
```

### Оплата на сайте

В файле обработки оплат после успешной оплаты устанавливаем пользовательскую переменную в Mailganer "Покупал на сайте":

```php
// Отправляем информацию о покупке в Mailganer
MailganerService::preparePayment($this->user->GetEmail());
```

### Регистрация на мероприятия

В файле обработки регистраций на мероприятия после успешной регистрации устанавливаем пользовательскую переменную в Mailganer "Регистрировался на мероприятия":
```php
// Отправляем информацию о регистрации в Mailganer
MailganerService::prepareRegistrationEvent($this->user->GetEmail());
```