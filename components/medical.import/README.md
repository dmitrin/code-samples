# Интеграция с 2GIS

Компонент необходим для загрузки файла с объектами в административной панели, его парсинга и интеграции с 2GIS.

Компонент используется в административной панели на отдельной странице. Загружается файл из различных объектов (файл состоит из порядка 30 тысяч записей). Происходит пошаговая обработка файла. На каждом шаге определяются уже имеющиеся объекты (чтобы не добавлять их снова) и уже имеющиеся адреса (чтобы не делать лишние запросы к API). Если ранее не было адреса в базе, то выполняется запрос к API 2GIS по определению точек координат по адресу. Если текущий адрес входит в список городов с метро, то делается дополнительный запрос к 2GIS для получения ближайших станций метро.
