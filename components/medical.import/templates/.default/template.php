<?php
$aTabs = [
    [
        "DIV" => "edit1",
        "TAB" => "Загрузка объектов на карту",
        "ICON" => "main_user_edit",
        "TITLE" => "Загрузка объектов на карту",
    ]
];
$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
$tabControl->BeginNextTab();
?>
<form id="uploader" name="uploader" method="post" class="medical_wrapper" enctype="multipart/form-data">
    <tr class="adm-detail-required-field">
        <td width="40%" class="adm-detail-content-cell-l">
            Файл для загрузки:
        </td>
        <td width="60%" class="adm-detail-content-cell-r">
            <input id="file" required type="file" name="file">
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <input class="adm-btn-save" type="submit" value="Запустить парсинг">
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <div id="logger_text" class="logger_text"></div>
        </td>
    </tr>
</form>
<? $tabControl->EndTab(); ?>
<? $tabControl->End(); ?>
<div class="adm-info-message-wrap medical_wrapper">
    <div class="adm-info-message">
        <p>Загружаемый файл должен иметь определенную структуру:</p>
        <table>
            <thead>
            <tr>
                <th>CITYTYPE</th>
                <th style="min-width: 130px;">CITYNAME</th>
                <th style="min-width: 140px;">REGIONLNAME</th>
                <th>AGENAME</th>
                <th>MEDCARENAME</th>
                <th>LADDRNAME</th>
                <th>LNAME</th>
                <th style="min-width: 100px;">DOSTUP</th>
                <th>TARIF_REGION</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Город</td>
                <td>САНКТ-ПЕТЕРБУРГ</td>
                <td>САНКТ-ПЕТЕРБУРГ Г</td>
                <td>Взрослые</td>
                <td>СТОМАТОЛОГИЯ</td>
                <td>190000, г.Санкт-Петербург, ул.Композиторов, д.10</td>
                <td>Общество с ограниченной ответственностью "Медицинский центр Одонт"</td>
                <td>Прямой доступ</td>
                <td>Region 2</td>
            </tr>
            <tr>
                <td>Город</td>
                <td>САНКТ-ПЕТЕРБУРГ</td>
                <td>САНКТ-ПЕТЕРБУРГ Г</td>
                <td>Взрослые</td>
                <td>СТОМАТОЛОГИЯ</td>
                <td>190000, г.Санкт-Петербург, ул.Дыбенко, д.13, корп.5</td>
                <td>Общество с ограниченной ответственностью "Медицинский центр Одонт"</td>
                <td>Прямой доступ</td>
                <td>Region 2</td>
            </tr>
            <tr style="text-align: center">
                <td colspan="9">
                    <strong>...</strong>
                </td>
            </tr>
            </tbody>
        </table>
        <p>Пример файла - <a href="<?= $this->GetFolder(); ?>/template.xlsx">Скачать</a></p>
    </div>
</div>
