BX.ready(function () {
    const form = document.getElementById('uploader');
    window.isAgainCheck = false;

    form.addEventListener('submit', (e) => {
        e.preventDefault();

        let file = BX('file').files[0];
        if (typeof file == "undefined") {
            alert('Вы не выбрали файл для парсинга!');
            return;
        }

        BX.showWait();

        let startRow = '2';
        let data = new BX.ajax.FormData();
        data.append('medicalItems', file);
        let logger = document.getElementById('logger_text');
        logger.insertAdjacentHTML("beforeend", "<div class='success'>Парсер запущен</div>");

        startProcessImport(data, startRow);
    });

    function startProcessImport(data, startRow) {
        data.append('startRow', startRow);
        data.send(
            '/local/ajax/medical_import.php',
            function (resultJson) {
                // Успех
                window.isAgainCheck = false;
                let result = BX.parseJSON(resultJson);
                let logger = document.getElementById('logger_text');
                logger.insertAdjacentHTML("beforeend",
                    "<div class='success'>Обработаны строки: " + result.statistic.countItems + " из " + result.statistic.countAll + "</div>"
                );

                result.logger.forEach(function (item, i) {
                    logger.insertAdjacentHTML("beforeend", "<div class='" + item.type + "'>" + item.message + "</div>");
                });
                logger.insertAdjacentHTML("beforeend", "<br>");

                let newStartRow = result.statistic.newStartRow;
                if (newStartRow < result.statistic.countAll) {
                    startProcessImport(data, newStartRow);
                } else {
                    logger.insertAdjacentHTML("beforeend",
                        "<div class='success'>Обработка завершена</div>"
                    );
                    BX.closeWait();
                }
            },
            null,
            function () {
                // Ошибка
                let logger = document.getElementById('logger_text');
                logger.insertAdjacentHTML("beforeend", "<div class='error'>Произошла ошибка!</div>");

                if (!window.isAgainCheck) {
                    // Пробуем повторно отправить запрос
                    window.isAgainCheck = true;
                    logger.insertAdjacentHTML("beforeend", "<br><div>Повторный запрос...</div>");
                    startProcessImport(data, startRow);
                } else {
                    logger.insertAdjacentHTML("beforeend", "<div class='error'>Операция прервана</div>");
                    BX.closeWait();
                }
            }
        );
    }
})
