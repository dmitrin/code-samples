<?php

use App\BaseComponent;
use App\Helpers\HighLoadBlock;
use Bitrix\Main\Loader;
use Bitrix\Main\Application;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Bitrix\Main\Web\HttpClient;
use CEventLog, COption;


/**
 * Компонент импорта Excel-файла с объектами
 */
class MedicalImportComponent extends BaseComponent
{
    const HT_MEDICAL_CITIES = 'MedicalCities';
    const HT_MEDICAL_ITEMS = 'MedicalItems';

    const LOG_TYPE_ERROR = 'error';
    const LOG_TYPE_SUCCESS = 'success';
    const JSON_PATH = '/upload/tmp/file_data.json';

    const COUNT_ROWS = 200;
    const INIT_ROW = 1;
    const SUBWAY_RADIUS = 200;
    const SUBWAY_MAX_ITEMS = 5;
    const STOP_SCRIPT = 150000;

    private $logger;
    private $statistic;

    private $jsonPath;
    private $apiUrl;
    private $apiKey;

    /**
     * Подготовка параметров компонента
     *
     * @param $arParams
     *
     * @return array
     *
     * @throws Exception
     */
    public function onPrepareComponentParams($arParams)
    {
        if (!Loader::includeModule('highloadblock')) {
            throw new \Exception('Модуль highloadblock не установлен');
        }

        $this->logger = [];
        $this->statistic = [];

        $this->jsonPath = $_SERVER['DOCUMENT_ROOT'] . self::JSON_PATH;
        $this->apiUrl = COption::GetOptionString('askaron.settings', 'UF_2GIS_URL');
        $this->apiKey = COption::GetOptionString('askaron.settings', 'UF_2GIS_KEY');

        $arParams = parent::onPrepareComponentParams($arParams);
        return $arParams;
    }

    /**
     * Выполнение компонента
     *
     * @return void|boolean
     */
    public function executeComponent()
    {
        if (!$this->user->IsAuthorized()) {
            return false;
        }

        if (!empty($this->arParams['ACTION'])) {
            $request = Application::getInstance()->getContext()->getRequest();
            switch ($this->arParams['ACTION']) {
                case 'processFile':
                    $this->processFile($request->getFile('medicalItems'), $request->getPost('startRow'));
                    break;
            }

            $this->returnResult([
                'statistic' => $this->statistic,
                'logger' => $this->logger
            ]);
        }

        if (file_exists($this->jsonPath)) {
            unlink($this->jsonPath);
        }

        $this->IncludeComponentTemplate();
    }

    /**
     * Обработка загруженного файла
     *
     * @param $file
     * @param $startRow
     *
     * @return void|boolean
     */
    public function processFile($file, $startRow)
    {
        $this->addLogJournal('Начало обработки файла. Итерация: ' . $startRow);
        if (empty($file)) {
            $this->returnError('Не был выбран файл');
        }

        $startRow = empty($startRow) ? self::INIT_ROW : $startRow - 1;
        $dataXls = [];

        if (!file_exists($this->jsonPath)) {
            try {
                $spreadsheet = IOFactory::load($file['tmp_name']);
                $spreadsheet = $spreadsheet->getActiveSheet();
                $dataXls = $spreadsheet->toArray();

                file_put_contents($this->jsonPath, json_encode($dataXls));
            } catch (Exception $ex) {
                $this->returnError('Не удалось обработать файл');
            }
        } else {
            $dataXls = json_decode(file_get_contents($this->jsonPath), true);
        }

        if (empty($dataXls)) {
            $this->returnError('Не удалось получить данные из файла');
        }

        $countAll = count($dataXls);

        $cities = [];
        $ages = [];
        $categories = [];
        $items = [];

        for ($i = $startRow; $i <= $startRow + self::COUNT_ROWS && $i < $countAll; $i++) {
            $currentRow = $dataXls[$i];
            $cities[] = $currentRow[1];
            $ages[] = $currentRow[3];
            $categories[] = $currentRow[4];

            $items[] = [
                'CITY' => $currentRow[1],
                'AGE' => $currentRow[3],
                'CATEGORY' => $currentRow[4],
                'ADDRESS' => $currentRow[5],
                'NAME' => $currentRow[6],
            ];
        }

        $agesIds = HighLoadBlock::getEnumIdsByValue($ages, 'UF_AGE');
        $categoriesIds = HighLoadBlock::getEnumIdsByValue($categories, 'UF_CATEGORY');

        $citiesIds = $this->processCities($cities);
        if (empty($citiesIds)) {
            $this->returnError('Произошла ошибка при попытке получить ID городов');
        }
        $citiesIdsWithSubway = $this->getCitiesWithSubway($citiesIds);

        foreach ($items as $key => $item) {
            $items[$key]['ID_CITY'] = $citiesIds[$item['CITY']];
            $items[$key]['ID_AGE'] = $agesIds[$item['AGE']];
            $items[$key]['ID_CATEGORY'] = $categoriesIds[$item['CATEGORY']];
        }

        $resultProcess = $this->processItems($items, $citiesIdsWithSubway);
        if (empty($resultProcess)) {
            $this->returnError('Произошла ошибка при обработке объектов');
        }

        $processRows = $startRow + self::COUNT_ROWS;
        $this->statistic['countAll'] = $countAll;
        $this->statistic['countItems'] = ($processRows < $countAll) ? $processRows : $countAll;
        $this->statistic['newStartRow'] = count($items) + $startRow;

        if ($processRows >= $countAll) {
            unlink($this->jsonPath);
        }

        if (!empty($this->statistic['COUNT_API_COORDINATE'])) {
            $this->addLogAdmin('Количество обращений к API координат: ' . $this->statistic['COUNT_API_COORDINATE'], self::LOG_TYPE_SUCCESS);
        }

        if (!empty($this->statistic['COUNT_API_METRO'])) {
            $this->addLogAdmin('Количество обращений к API метро: ' . $this->statistic['COUNT_API_METRO'], self::LOG_TYPE_SUCCESS);
        }

        if (!empty($this->statistic['ADDED_ITEMS'])) {
            $this->addLogAdmin('Тот же самый адрес в одной итерации: ' . $this->statistic['ADDED_ITEMS'], self::LOG_TYPE_SUCCESS);
        }

        if (!empty($this->statistic['EXIST_ITEMS'])) {
            $this->addLogAdmin('Адрес уже был в БД: ' . $this->statistic['EXIST_ITEMS'], self::LOG_TYPE_SUCCESS);
        }

        $this->addLogJournal('Конец обработки файла');
    }

    /**
     * Добавление городов
     *
     * @param $cities
     *
     * @return array|boolean
     */
    public function processCities($cities)
    {
        $entityDataClass = HighLoadBlock::getDataClassByName(self::HT_MEDICAL_CITIES);
        if (empty($entityDataClass)) {
            return false;
        }

        // Получение существующих городов
        $cities = array_unique($cities);
        $existCities = [];
        $citiesResource = $entityDataClass::getList();
        while ($city = $citiesResource->Fetch()) {
            $existCities[$city['ID']] = $city['UF_NAME'];
        }

        // Добавление новых городов
        $counterAdd = 0;
        $addCities = array_diff($cities, $existCities);
        foreach ($addCities as $city) {
            $result = $entityDataClass::add([
                'UF_NAME' => $city
            ]);
            if ($result->isSuccess()) {
                $counterAdd++;
                $existCities[$result->getId()] = $city;
            } else {
                $this->addLogAdmin('Не удалось добавить город: ' . $city, self::LOG_TYPE_ERROR);
            }
        }

        if (!empty($counterAdd)) {
            $this->addLogAdmin('Добавлено городов: ' . $counterAdd, self::LOG_TYPE_SUCCESS);
        }

        return array_flip($existCities);
    }

    /**
     * Формирование ID городов, у которых есть метро
     *
     * @param $citiesIds
     *
     * @return array
     */
    public function getCitiesWithSubway($citiesIds)
    {
        $citiesIdsWithSubway = [];
        $citiesWithSubway = [
            'МОСКВА',
            'САНКТ-ПЕТЕРБУРГ',
            'КАЗАНЬ',
            'ЕКАТЕРИНБУРГ',
            'НИЖНИЙ НОВГОРОД',
            'НОВОСИБИРСК',
            'САМАРА',
        ];

        foreach ($citiesIds as $key => $city) {
            if (in_array($key, $citiesWithSubway)) {
                $citiesIdsWithSubway[] = $city;
            }
        }

        return $citiesIdsWithSubway;
    }

    /**
     * Добавление объектов
     *
     * @param $items
     * @param $citiesIdsWithSubway
     *
     * @return boolean
     */
    public function processItems($items, $citiesIdsWithSubway)
    {
        $entityDataClass = HighLoadBlock::getDataClassByName(self::HT_MEDICAL_ITEMS);
        if (empty($entityDataClass)) {
            return false;
        }

        // Получение существующих объектов
        $existItems = [];
        $existItemsInfo = [];
        $itemsResource = $entityDataClass::getList();
        while ($item = $itemsResource->Fetch()) {
            $existItems[] = $item['UF_ID_CITY'] . $item['UF_ADDRESS'] . $item['UF_NAME'] . $item['UF_AGE'] . $item['UF_CATEGORY'];
            $existItemsInfo[$item['UF_ADDRESS']] = [
                'lat' => $item['UF_LAT'],
                'lon' => $item['UF_LON'],
                'subway' => $item['UF_SUBWAY'],
            ];
        }

        $addedItems = [];
        $counterAdd = 0;
        foreach ($items as $item) {
            $string = $item['ID_CITY'] . $item['ADDRESS'] . $item['NAME'] . $item['ID_AGE'] . $item['ID_CATEGORY'];
            if (!in_array($string, $existItems)) {
                if (!empty($addedItems[$item['ADDRESS']])) {
                    $this->statistic['ADDED_ITEMS']++;
                    $points = $addedItems[$item['ADDRESS']];
                } elseif (!empty($existItemsInfo[$item['ADDRESS']])) {
                    $this->statistic['EXIST_ITEMS']++;
                    $points = $existItemsInfo[$item['ADDRESS']];
                } else {
                    $points = $this->getItemPoints($item['ADDRESS'], $item['ID_CITY'], $citiesIdsWithSubway);
                    $addedItems[$item['ADDRESS']] = $points;
                }

                $result = $entityDataClass::add([
                    'UF_ID_CITY' => $item['ID_CITY'],
                    'UF_AGE' => $item['ID_AGE'],
                    'UF_CATEGORY' => $item['ID_CATEGORY'],
                    'UF_NAME' => $item['NAME'],
                    'UF_ADDRESS' => $item['ADDRESS'],
                    'UF_LAT' => $points['lat'],
                    'UF_LON' => $points['lon'],
                    'UF_SUBWAY' => $points['subway'],
                ]);

                if ($result->isSuccess()) {
                    $counterAdd++;
                } else {
                    $this->addLogAdmin('Не удалось добавить объект: ' . $item['NAME'], self::LOG_TYPE_ERROR);
                }
            }
        }

        if (!empty($counterAdd)) {
            $this->addLogAdmin('Добавлено объектов: ' . $counterAdd, self::LOG_TYPE_SUCCESS);
        }

        return true;
    }

    /**
     * Получение координат объекта
     *
     * @param $address
     * @param $idCity
     * @param $citiesIdsWithSubway
     *
     * @return array
     */
    public function getItemPoints($address, $idCity, $citiesIdsWithSubway)
    {
        $httpClient = new HttpClient();
        $lat = '';
        $lon = '';
        $subway = '';

        $pointsResult = $httpClient->get(
            $this->apiUrl . '?key=' . $this->apiKey . '&fields=items.point&type=building&q=' . $address
        );
        $pointsResult = json_decode($pointsResult, true);
        if (!empty($pointsResult['error']['message']) && $pointsResult['error']['type'] !== 'itemNotFound') {
            $this->addLogAdmin($pointsResult['error']['message'], self::LOG_TYPE_ERROR);
        }
        // Так как есть ограничение API (RPS = 10), то на 0.15 секунд замедляем скрипт
        usleep(self::STOP_SCRIPT);
        $this->statistic['COUNT_API_COORDINATE']++;

        $points = current($pointsResult['result']['items'])['point'];
        if (!empty($points['lat']) && !empty($points['lon'])) {
            $lat = $points['lat'];
            $lon = $points['lon'];

            // Если объект в городе, где есть метро, пробуем получить
            if (in_array($idCity, $citiesIdsWithSubway)) {
                $subway = $this->getItemSubway($lat, $lon);
            }
        } else {
            $this->addLogAdmin('Не удалось получить координаты для элемента с адресом: ' . $address, self::LOG_TYPE_ERROR);
        }

        return [
            'lat' => $lat,
            'lon' => $lon,
            'subway' => $subway
        ];
    }

    /**
     * Получение ближайших станций метро
     *
     * @param $lat
     * @param $lon
     *
     * @return string
     */
    public function getItemSubway($lat, $lon)
    {
        $subway = '';
        $httpClient = new HttpClient();

        $subwayResult = $httpClient->get(
            $this->apiUrl . '?key=' . $this->apiKey . '&q=метро&radius=' . self::SUBWAY_RADIUS . '&point=' . $lon . ',' . $lat
        );
        $subwayResult = json_decode($subwayResult, true);
        // Так как есть ограничение API (RPS = 10), то на 0.15 секунд замедляем скрипт
        usleep(self::STOP_SCRIPT);
        $this->statistic['COUNT_API_METRO']++;

        if (!empty($subwayResult['result']['items'])) {
            $subwayItems = [];
            foreach ($subwayResult['result']['items'] as $subway) {
                if ($subway['type'] === 'station' && $subway['subtype'] === 'metro') {
                    $subwayItems[] = $subway['name'];
                }
            }
            $subwayItems = array_unique($subwayItems);
            $subwayItems = array_slice($subwayItems, 0, self::SUBWAY_MAX_ITEMS);
            $subway = implode(',', $subwayItems);
        }

        return $subway;
    }

    /**
     * Добавление данных в логи для отображения пользователю
     *
     * @param $message
     * @param $type
     *
     * @return void
     */
    public function addLogAdmin($message, $type)
    {
        $this->logger[] = [
            'type' => $type,
            'message' => $message
        ];
    }

    /**
     * Логирование в журнал событий
     *
     * @param $description
     *
     * @return void
     */
    public static function addLogJournal($description)
    {
        CEventLog::Add([
            'SEVERITY' => 'DEBUG',
            'AUDIT_TYPE_ID' => 'MEDICAL_MAP',
            'MODULE_ID' => 'main',
            'DESCRIPTION' => $description . '. Время: ' . date('d.m.Y H:i:s'),
        ]);
    }

    /**
     * Возврат результата
     *
     * @param $result
     *
     * @return void
     */
    public function returnResult($result)
    {
        header('Content-Type: application/json');
        die(json_encode($result, JSON_UNESCAPED_UNICODE));
    }

    /**
     * Возврат ошибки
     *
     * @param $messageToLog
     *
     * @return void
     */
    public function returnError($messageToLog = '')
    {
        if (!empty($messageToLog)) {
            $this->addLogJournal($messageToLog);
        }

        header('HTTP/1.1 400 Internal Server');
        die();
    }
}
