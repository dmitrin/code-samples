<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
$this->setFrameMode(false);

if (empty($arResult['PACKAGES']) || !empty($arResult['NEED_AUTH'])) {
    return;
}
?>
<application
    ref="applicationComponent"
    title="<?= $arParams['TITLE']; ?>"
    step-name="<?= $arParams['STEP_NAME']; ?>"
    element-id="<?= $arParams['ELEMENT_ID']; ?>"
    element-code="<?= $arParams['CODE_SECTION']; ?>"
    element-name="<?= $arParams['ELEMENT_NAME']; ?>"
    dadata-api-key="<?= $arParams['DADATA_TOKEN']; ?>"
    re-captcha-key="<?= $arParams['RE_CAPTCHA_KEY']; ?>"
    :packages="<?= htmlspecialchars(json_encode($arResult['PACKAGES'], JSON_UNESCAPED_UNICODE)); ?>"
    <? if (!empty($arParams['IS_FULL_APPLICATION'])): ?>
        is-full-application
    <? endif; ?>
    <? if (!empty($arParams['IS_FREE_APPLICATION'])): ?>
        is-free-application
    <? endif; ?>
    <? if (!empty($arResult['PAYMENT_METHODS'])): ?>
        :payment-methods="<?= htmlspecialchars(json_encode($arResult['PAYMENT_METHODS'], JSON_UNESCAPED_UNICODE)); ?>"
    <? endif; ?>
    action-add-application="/bitrix/services/main/ajax.php?mode=class&c=core:sber.acquiring&action=addApplication"
    action-add-payment-online="/bitrix/services/main/ajax.php?mode=class&c=core:sber.acquiring&action=addPaymentOnline"
    action-add-payment-bill="/bitrix/services/main/ajax.php?mode=class&c=core:sber.acquiring&action=addPaymentBill"
    action-load-excel-users="/bitrix/services/main/ajax.php?mode=class&c=core:sber.acquiring&action=loadExcelUsers"
    action-current-user="/bitrix/services/main/ajax.php?mode=class&c=core:sber.acquiring&action=getCurrentUser"
></application>
