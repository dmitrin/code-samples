<?php

use App\{BaseComponent, Config};
use App\Helpers\{Iblock, Text, Url};
use App\Helpers\User as UserHelper;
use App\Services\UserService;
use App\Services\UserLogService\UserLogTable;
use Bitrix\Main\{Loader, Error, ErrorCollection};
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\Response\AjaxJson;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Mail\Event;
use User, CIBlockPropertyEnum, CIBlockElement, CIBlockSection, CFile, CMain, COption, Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;


/**
 * Компонент интеграции со Сбербанк Эквайрингом
 */
class SberAcquiringComponent extends BaseComponent implements Controllerable
{
    const IBLOCK_PACKAGES_CODE = 'acquiring_packages';
    const IBLOCK_APPLICATIONS_CODE = 'acquiring_applications';
    const IBLOCK_PAYMENTS_CODE = 'acquiring_payments';

    const CODE_ONLINE_PAYMENT_METHOD = 'METHOD';

    public function onPrepareComponentParams($arParams): array
    {
        Loader::includeModule('iblock');

        $arParams['SBER_TOKEN'] = COption::GetOptionString('askaron.settings', 'UF_SBER_PAY_TOKEN');
        $arParams['SBER_PATH'] = COption::GetOptionString('askaron.settings', 'UF_SBER_PAY_PATH');
        $arParams['DADATA_TOKEN'] = Config::getDadataToken();
        $arParams['RE_CAPTCHA_KEY'] = Config::getReCaptchaApiKey();

        $arParams['IBLOCK_PACKAGES'] = Iblock::getIblockIdByCode(self::IBLOCK_PACKAGES_CODE);
        $arParams['IBLOCK_APPLICATIONS'] = Iblock::getIblockIdByCode(self::IBLOCK_APPLICATIONS_CODE);
        $arParams['IBLOCK_PAYMENTS'] = Iblock::getIblockIdByCode(self::IBLOCK_PAYMENTS_CODE);

        $arParams['APPLE_MERCHANT'] = COption::GetOptionString('askaron.settings', 'UF_APPLE_MERCHANT');
        $arParams['APPLE_TOKEN'] = COption::GetOptionString('askaron.settings', 'UF_APPLE_TOKEN');
        $arParams['GOOGLE_MERCHANT'] = COption::GetOptionString('askaron.settings', 'UF_GOOGLE_MERCHANT');
        $arParams['GOOGLE_TOKEN'] = COption::GetOptionString('askaron.settings', 'UF_GOOGLE_TOKEN');

        $arParams['ELEMENT_ID'] = !empty($arParams['ELEMENT_ID']) ? (int)$arParams['ELEMENT_ID'] : false;
        $arParams['IS_FULL_APPLICATION'] = !empty($arParams['IS_FULL_APPLICATION']);
        $arParams['IS_FREE_APPLICATION'] = !empty($arParams['IS_FREE_APPLICATION']);

        $arParams = parent::onPrepareComponentParams($arParams);

        return $arParams;
    }

    public function executeComponent(): void
    {
        if (!$this->user->IsAuthorized()) {
            $this->arResult['NEED_AUTH'] = true;
        }

        $this->arResult['PACKAGES'] = $this->getPackages($this->arParams['ELEMENT_ID']);
        $this->arResult['PAYMENT_METHODS'] = $this->getPaymentMethods();

        $this->includeComponentTemplate();
    }

    /**
     * Определение endpoint's для компонента
     *
     * @return array
     */
    public function configureActions()
    {
        return [
            'addApplication' => [
                'prefilters' => [],
                'postfilters' => []
            ],
            'addPaymentOnline' => [
                'prefilters' => [],
                'postfilters' => []
            ],
            'addPaymentBill' => [
                'prefilters' => [],
                'postfilters' => []
            ],
            'loadExcelUsers' => [
                'prefilters' => [],
                'postfilters' => []
            ],
            'getCurrentUser' => [
                'prefilters' => [],
                'postfilters' => []
            ],
            'getBillInfo' => [
                'prefilters' => [],
                'postfilters' => []
            ],
        ];
    }

    /**
     * Создание заявки на оплату
     *
     * @param $elementId
     * @param $elementCode
     * @param $packageId
     * @param $isAnotherUser
     * @param $isFreeApplication
     * @param $users
     * @param $applicationId
     *
     * @return array
     */
    public function addApplicationAction(
        $elementId,
        $elementCode,
        $packageId,
        $isAnotherUser,
        $isFreeApplication,
        $users,
        $applicationId = ''
    )
    {
        // Заявка может быть создана только авторизованным пользователем
        $currentUserId = $this->user->GetID();
        if (empty($currentUserId)) {
            return self::returnError(Loc::getMessage('ERROR_APPLICATION_AUTH'));
        }

        // Формирование информации о пользователях
        $currentUsers = $users;
        if (empty($currentUsers)) {
            return self::returnError(Loc::getMessage('ERROR_CURRENT_USERS'));
        }

        $userTable = [];
        $userIds = [];

        foreach ($currentUsers as $key => $user) {
            $userTable['company'][] = $user['company'];
            $userTable['name'][] = $user['name'];
            $userTable['surname'][] = $user['surname'];
            $userTable['middleName'][] = $user['middleName'];
            $userTable['email'][] = $user['email'];
            $userTable['phone'][] = $user['phone'];
            $userTable['position'][] = $user['position'];
            $userTable['city'][] = $user['city'];

            // Проверка наличия пользователя в БД по E-mail
            $userCheckId = UserHelper::getUserIdByEmail($user['email']);
            if ($userCheckId) {
                $userId = $userCheckId;
                $userIds[] = $userId;
            } else {
                // Создание личного кабинета для пользователя
                $userObject = new CUser;
                $user['password'] = Text::generatePassword(8);
                $user['confirmCode'] = randString(8);

                $addUserFields = [
                    'NAME' => $user['name'],
                    'LAST_NAME' => $user['surname'],
                    'SECOND_NAME' => $user['middleName'],
                    'EMAIL' => $user['email'],
                    'LOGIN' => $user['email'],
                    'ACTIVE' => 'N',
                    'PASSWORD' => $user['password'],
                    'CONFIRM_PASSWORD' => $user['password'],
                    'PERSONAL_MOBILE' => $user['phone'],
                    'PERSONAL_CITY' => $user['city'],
                    'WORK_POSITION' => $user['position'],
                    'WORK_COMPANY' => $user['company'],
                    'CONFIRM_CODE' => $user['confirmCode'],
                ];

                $userId = $userObject->Add($addUserFields);
                if (empty($userId)) {
                    return self::returnError(Loc::getMessage('ERROR_USER_ADD') . $user['email']);
                }

                $user['ID'] = $userId;
                self::sendMessageToNewUser($user, $elementId, $packageId);

                $userIds[] = $userId;
            }

            // Проверка на наличие файлов
            self::checkAndSaveUserFile('education', 'UF_DIPLOMA', $userId, $key);
            self::checkAndSaveUserFile('passport', 'UF_PASSPORT', $userId, $key);
        }

        if (empty($userTable)) {
            return self::returnError(Loc::getMessage('ERROR_USER_TABLE'));
        }

        // Получение общей стоимости
        $amount = self::getPackageAmount($packageId, count($currentUsers));

        $sectionId = Iblock::getSectionIdByCode($elementCode, $this->arParams['IBLOCK_APPLICATIONS']);
        if (empty($sectionId)) {
            return self::returnError(Loc::getMessage('ERROR_SECTION_NOT_FOUND'));
        }

        $isHelp = false;
        if (!empty($isAnotherUser) && $isAnotherUser != 'false') {
            $isHelp = Iblock::getPropertyEnumIdByValue('Y', 'IS_HELP', $this->arParams['IBLOCK_APPLICATIONS']);
        }

        $isFree = false;
        if (!empty($isFreeApplication) && $isFreeApplication != 'false') {
            $isFree = Iblock::getPropertyEnumIdByValue('Y', 'IS_FREE', $this->arParams['IBLOCK_APPLICATIONS']);
        }

        $elementObject = new CIBlockElement;
        $applicationProperties = [
            'ELEMENT_PAY' => $elementId,
            'PACKAGE' => $packageId,
            'AMOUNT' => empty($isFree) ? $amount : 0,
            'IS_HELP' => $isHelp,
            'IS_FREE' => $isFree,
            'USER_MAIN' => $currentUserId,
            'USERS' => $userIds,
            'USERS_INFO' => [$userTable],
        ];

        if (empty($applicationId)) {
            // Добавление заявки
            $applicationFields = [
                'ACTIVE' => 'Y',
                'IBLOCK_ID' => $this->arParams['IBLOCK_APPLICATIONS'],
                'MODIFIED_BY' => $this->user->GetID(),
                'NAME' => 'Заявка на оплату от ' . FormatDate('FULL', false, true),
                'DATE_ACTIVE_FROM' => FormatDate('FULL', false, true),
                'IBLOCK_SECTION_ID' => $sectionId,
                'PROPERTY_VALUES' => $applicationProperties,
            ];

            $result = $elementObject->Add($applicationFields);
        } else {
            // Обновление заявки
            $result = $elementObject->Update($applicationId, ['PROPERTY_VALUES' => $applicationProperties]);
        }

        if ($result) {
            return [
                'applicationId' => !empty($applicationId) ? $applicationId : (string)$result,
            ];
        } else {
            return self::returnError(Loc::getMessage('ERROR_APPLICATION'));
        }
    }

    /**
     * Создание заявки на оплату (онлайн-платеж)
     *
     * @param $applicationId
     * @param $paymentMethod
     *
     * @return array
     */
    public function addPaymentOnlineAction($applicationId, $paymentMethod)
    {
        if (empty($applicationId)) {
            return self::returnError(Loc::getMessage('ERROR_APPLICATION_ID'));
        }

        // Получение информации о заявке на оплату
        $applicationInfo = self::getApplicationInfo($applicationId);
        if (empty($applicationInfo)) {
            return self::returnError(Loc::getMessage('ERROR_APPLICATION_FOUND'));
        }

        // Получение ID раздела
        $sectionId = self::getSectionIdByApplicationSection($applicationInfo['SECTION_ID']);
        if (empty($sectionId)) {
            return self::returnError(Loc::getMessage('ERROR_SECTION_NOT_FOUND'));
        }

        // Получение дополнительных данных
        $applicationInfo['PROPERTIES']['TYPE'] = Iblock::getPropertyEnumIdByXml('online', $this->arParams['IBLOCK_PAYMENTS']);
        $applicationInfo['PROPERTIES']['STATUS'] = Iblock::getPropertyEnumIdByXml('not_paid', $this->arParams['IBLOCK_PAYMENTS']);
        $applicationInfo['PROPERTIES']['METHOD'] = Iblock::getPropertyEnumIdByXml($paymentMethod, $this->arParams['IBLOCK_PAYMENTS']);

        $elementObject = new CIBlockElement;
        $paymentFields = [
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $this->arParams['IBLOCK_PAYMENTS'],
            'MODIFIED_BY' => $this->user->GetID(),
            'NAME' => 'Оплата заявки ' . $applicationId,
            'DATE_ACTIVE_FROM' => FormatDate('FULL', false, true),
            'IBLOCK_SECTION_ID' => $sectionId,
            'PROPERTY_VALUES' => $applicationInfo['PROPERTIES'],
        ];

        $resultAdd = $elementObject->Add($paymentFields);
        if (!$resultAdd) {
            return self::returnError(Loc::getMessage('ERROR_PAYMENT'));
        }

        $client = Config::getSberAcquiringClient();
        $returnUrl = (CMain::IsHTTPS() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/pay-result/';
        $amount = $applicationInfo['PROPERTIES']['AMOUNT'] * 100;
        $elementName = Iblock::getElementNameById($applicationInfo['PROPERTIES']['ELEMENT_PAY']);
        $packageName = Iblock::getElementNameById($applicationInfo['PROPERTIES']['PACKAGE']);
        $data = [
            'jsonParams' => [
                'payType' => $paymentMethod,
                'url' => Text::removeGetParams($_SERVER['HTTP_REFERER'])
            ],
            'description' => $elementName . ', ' . $packageName
        ];

        switch ($paymentMethod) {
            case 'card':
                $resultPayment = $client->registerOrder($resultAdd, $amount, $returnUrl, $data);
                break;
            case 'apple_pay':
                $resultPayment = $client->payWithApplePay($resultAdd, $this->arParams['APPLE_MERCHANT'], $this->arParams['APPLE_TOKEN']);
                break;
            case 'google_pay':
                $resultPayment = $client->payWithGooglePay($resultAdd, $this->arParams['GOOGLE_MERCHANT'], $this->arParams['GOOGLE_TOKEN']);
                break;
            default:
                return self::returnError(Loc::getMessage('ERROR_PAYMENT_METHOD'));
                break;
        }

        if (!empty($resultPayment['formUrl'])) {
            return [
                'paymentLink' => $resultPayment['formUrl']
            ];
        } else {
            return self::returnError(Loc::getMessage('ERROR_PAYMENT'));
        }
    }

    /**
     * Создание заявки на оплату (по счету)
     *
     * @param $applicationId
     * @param $userType
     * @param $paymentInfo
     * @param $paymentId
     *
     * @return array
     */
    public function addPaymentBillAction($applicationId, $userType, $paymentInfo, $paymentId = '')
    {
        if (empty($applicationId)) {
            return self::returnError(Loc::getMessage('ERROR_APPLICATION_ID'));
        }

        // Получение информации о заявке на оплату
        $applicationInfo = self::getApplicationInfo($applicationId);
        if (empty($applicationInfo)) {
            return self::returnError(Loc::getMessage('ERROR_APPLICATION_FOUND'));
        }

        // Получение ID раздела
        $sectionId = self::getSectionIdByApplicationSection($applicationInfo['SECTION_ID']);
        if (empty($sectionId)) {
            return self::returnError(Loc::getMessage('ERROR_SECTION_NOT_FOUND'));
        }

        // Получение типа платежа
        $paymentType = Iblock::getPropertyEnumIdByXml('bill', $this->arParams['IBLOCK_PAYMENTS']);
        if (!empty($paymentType)) {
            $applicationInfo['PROPERTIES']['TYPE'] = $paymentType;
        } else {
            return self::returnError(Loc::getMessage('ERROR_PAYMENT_TYPE'));
        }

        // Получение дополнительных данных
        $userTypeId = false;
        switch ($userType) {
            case 'entity':
                $userTypeId = Iblock::getPropertyEnumIdByXml('entity', $this->arParams['IBLOCK_PAYMENTS']);
                break;
            case 'individual':
                $userTypeId = Iblock::getPropertyEnumIdByXml('individual', $this->arParams['IBLOCK_PAYMENTS']);
                break;
        }
        $applicationInfo['PROPERTIES']['USER_TYPE'] = $userTypeId;
        $applicationInfo['PROPERTIES']['STATUS'] = Iblock::getPropertyEnumIdByXml('not_paid', $this->arParams['IBLOCK_PAYMENTS']);

        // Формирование данных для оплаты
        $informationMatches = [
            'company' => 'LEGAL_COMPANY',
            'ogrn' => 'LEGAL_OGRN',
            'inn' => 'LEGAL_INN',
            'kpp' => 'LEGAL_KPP',
            'legalAddress' => 'LEGAL_ADDRESS',
            'factAddress' => 'LEGAL_ADDRESS_FACT',
            'addressRegistration' => 'INDIVIDUAL_ADDRESS',
            'individualInn' => 'INDIVIDUAL_INN',
        ];
        $information = json_decode($paymentInfo);
        foreach ($information as $keyInfo => $valueInfo) {
            if (!empty($informationMatches[$keyInfo])) {
                $applicationInfo['PROPERTIES'][$informationMatches[$keyInfo]] = $valueInfo;
            }
        }

        $elementObject = new CIBlockElement;
        if (empty($paymentId)) {
            // Создание оплаты
            $paymentFields = [
                'ACTIVE' => 'Y',
                'IBLOCK_ID' => $this->arParams['IBLOCK_PAYMENTS'],
                'MODIFIED_BY' => $this->user->GetID(),
                'NAME' => 'Оплата заявки ' . $applicationId,
                'DATE_ACTIVE_FROM' => FormatDate('FULL', false, true),
                'IBLOCK_SECTION_ID' => $sectionId,
                'PROPERTY_VALUES' => $applicationInfo['PROPERTIES'],
            ];
            $result = $elementObject->Add($paymentFields);

            // Сохранение информации в журнал активности
            try {
                UserLogTable::create($applicationInfo['PROPERTIES']['USER'], [
                    'id' => $applicationInfo['PROPERTIES']['ELEMENT_PAY'],
                    'type' => Iblock::getSectionCodeById($sectionId),
                    'idPayment' => $result
                ]);
            } catch (Exception $exception) {
                return self::returnError($exception->getMessage());
            }
        } else {
            // Обновление существующей оплаты
            $result = $elementObject->Update($paymentId, ['PROPERTY_VALUES' => $applicationInfo['PROPERTIES']]) ? $paymentId : false;
        }

        if ($result) {
            $applicationInfo['PROPERTIES']['ACTIVE_FROM'] = FormatDate('FULL', false, true);
            return self::generateBillData($result, $applicationInfo['ALL_PROPERTIES'], $applicationInfo['PROPERTIES'], $userType);
        } else {
            return self::returnError(Loc::getMessage('ERROR_PAYMENT'));
        }
    }

    /**
     * Множественная загрузка пользователей из Excel-файла
     *
     * @return array
     */
    public function loadExcelUsersAction()
    {
        $result = [];
        $dataXls = [];
        $file = $_FILES['file'];

        if (!empty($file)) {
            $spreadsheet = IOFactory::load($file['tmp_name']);
            $spreadsheet = $spreadsheet->getActiveSheet();
            $dataXls = $spreadsheet->toArray();
        }

        if (!is_array($dataXls)) {
            return self::returnError(Loc::getMessage('ERROR_EXCEL_USERS'));
        }

        $header = array_shift($dataXls);
        if (!is_array($header)) {
            return self::returnError(Loc::getMessage('ERROR_EXCEL_USERS'));
        }

        $arKeyFields = [
            'Название компании' => 'company',
            'Имя' => 'name',
            'Фамилия' => 'surname',
            'Отчество' => 'middleName',
            'E-mail' => 'email',
            'Номер телефона' => 'phone',
            'Должность' => 'position',
            'Город' => 'city',
        ];
        foreach ($header as $key => $headerValue) {
            if (!empty($headerValue) && !empty($arKeyFields[$headerValue])) {
                $header[$key] = $arKeyFields[$headerValue];
            }
        }

        foreach ($dataXls as $key => $arrValue) {
            $result[] = array_combine($header, $arrValue);
        }

        foreach ($result as $keyUser => $valueUser) {
            foreach ($valueUser as $keyField => $valueField) {
                if (empty($keyField)) {
                    unset($result[$keyUser][$keyField]);
                }
            }
        }

        return [
            'users' => $result
        ];
    }

    /**
     * Формирование счета по ID оплаты
     *
     * @param $paymentId
     *
     * @return array
     */
    public function getBillInfoAction($paymentId)
    {
        $currentUserId = $this->user->GetID();
        if (empty($currentUserId)) {
            return self::returnError(Loc::getMessage('ERROR_USER_AUTH'));
        }

        $paymentFilter = [
            'IBLOCK_ID' => $this->arParams['IBLOCK_PAYMENT'],
            'ACTIVE' => 'Y',
            'PROPERTY_USER' => $currentUserId,
            '=ID' => $paymentId,
        ];
        $paymentResult = CIBlockElement::GetList([], $paymentFilter);
        if ($payment = $paymentResult->GetNextElement()) {
            $paymentFields = $payment->GetFields();
            $paymentProperties = $payment->GetProperties();
            $paymentData = [
                'LEGAL_COMPANY' => $paymentProperties['LEGAL_COMPANY']['VALUE'],
                'LEGAL_INN' => $paymentProperties['LEGAL_INN']['VALUE'],
                'LEGAL_KPP' => $paymentProperties['LEGAL_KPP']['VALUE'],
                'LEGAL_ADDRESS' => $paymentProperties['LEGAL_ADDRESS']['VALUE'],
                'INDIVIDUAL_INN' => $paymentProperties['INDIVIDUAL_INN']['VALUE'],
                'INDIVIDUAL_ADDRESS' => $paymentProperties['INDIVIDUAL_ADDRESS']['VALUE'],
                'APPLICATION' => $paymentProperties['APPLICATION']['VALUE'],
                'USER_TYPE' => $paymentProperties['USER_TYPE']['VALUE_XML_ID'],
                'ACTIVE_FROM' => $paymentFields['ACTIVE_FROM'],
            ];
        } else {
            return self::returnError(Loc::getMessage('ERROR_PAYMENT_NOT_FOUND'));
        }

        if (empty($paymentData['APPLICATION'])) {
            return self::returnError(Loc::getMessage('ERROR_APPLICATION_FOUND'));
        }

        $applicationInfo = self::getApplicationInfo($paymentData['APPLICATION']);
        if (empty($applicationInfo)) {
            return self::returnError(Loc::getMessage('ERROR_APPLICATION_FOUND'));
        }

        return self::generateBillData($paymentId, $applicationInfo['ALL_PROPERTIES'], $paymentData, $paymentData['USER_TYPE']);
    }

    /**
     * Получение пакетов для элемента
     *
     * @param $elementId
     *
     * @return array|boolean
     */
    public function getPackages($elementId)
    {
        $packages = [];

        $packagesSelect = [
            'ID', 'NAME', 'PREVIEW_TEXT', 'PROPERTY_COST'
        ];
        $packagesFilter = [
            'IBLOCK_ID' => $this->arParams['IBLOCK_PACKAGES'],
            'ACTIVE' => 'Y',
            '=PROPERTY_ELEMENT' => $elementId,
        ];
        $packagesResult = CIBlockElement::GetList(['SORT' => 'ASC'], $packagesFilter, false, false, $packagesSelect);
        while ($item = $packagesResult->GetNext()) {
            $packages[] = [
                'id' => $item['ID'],
                'name' => $item['NAME'],
                'text' => $item['PREVIEW_TEXT'],
                'cost' => empty($this->arParams['IS_FREE_APPLICATION']) && !empty($item['PROPERTY_COST_VALUE'])
                    ? $item['PROPERTY_COST_VALUE'] : 0,
            ];
        }

        return $packages;
    }

    /**
     * Получение общей суммы на оплату по выбранному пакету
     *
     * @param $packageId
     * @param $countUsers
     *
     * @return int
     */
    public function getPackageAmount($packageId, $countUsers)
    {
        $amount = 0;
        $packageSelect = [
            'ID', 'PROPERTY_COST'
        ];
        $packageFilter = [
            'IBLOCK_ID' => $this->arParams['IBLOCK_PACKAGES'],
            'ACTIVE' => 'Y',
            '=ID' => $packageId,
        ];

        $packageResult = CIBlockElement::GetList([], $packageFilter, false, false, $packageSelect);
        if ($package = $packageResult->GetNext()) {
            $amount = $package['PROPERTY_COST_VALUE'] * $countUsers;
        }

        return $amount;
    }

    /**
     * Получение информации о текущем пользователе
     *
     * @return array|boolean
     */
    public function getCurrentUserAction()
    {
        if (!$this->user->IsAuthorized()) {
            return false;
        }

        $user = $this->user->GetByID($this->user->GetID());
        $user = $user->Fetch();

        if (empty($user)) {
            return false;
        }

        $userInfo = [
            'id' => $user['ID'],
            'company' => $user['WORK_COMPANY'],
            'name' => $user['NAME'],
            'surname' => $user['LAST_NAME'],
            'middleName' => $user['SECOND_NAME'],
            'email' => $user['EMAIL'],
            'phone' => $user['PERSONAL_MOBILE'],
            'position' => $user['WORK_POSITION'],
            'city' => $user['PERSONAL_CITY'],
            'educationConfirm' => (bool)$user['UF_DIPLOMA_CONFIRMED'],
            'passportConfirm' => (bool)$user['UF_PASSPORT_CONFIRMED'],
        ];

        if (!empty($user['UF_DIPLOMA'])) {
            $userInfo['education'] = CFile::GetPath($user['UF_DIPLOMA']);
        }

        if (!empty($user['UF_PASSPORT'])) {
            $userInfo['passport'] = CFile::GetPath($user['UF_PASSPORT']);
        }

        return $userInfo;
    }

    /**
     * Получение способов онлайн оплаты
     *
     * @return array
     */
    public function getPaymentMethods()
    {
        $methods = [];

        $enumsResource = CIBlockPropertyEnum::GetList(
            [
                'SORT' => 'ASC'
            ], [
                'IBLOCK_ID' => $this->arParams['IBLOCK_PAYMENTS'],
                'CODE' => self::CODE_ONLINE_PAYMENT_METHOD
            ]
        );
        while ($enum = $enumsResource->GetNext()) {
            $methods[] = [
                'id' => $enum['ID'],
                'name' => $enum['VALUE'],
                'code' => $enum['XML_ID']
            ];
        }

        return $methods;
    }

    /**
     * Получение информации по заявке на оплату
     *
     * @param $applicationId
     *
     * @return array|boolean
     */
    public function getApplicationInfo($applicationId)
    {
        $applicationResult = CIBlockElement::GetList(
            [],
            [
                'ACTIVE' => 'Y',
                'IBLOCK_ID' => $this->arParams['IBLOCK_APPLICATIONS'],
                '=ID' => $applicationId,
            ]
        );
        if ($application = $applicationResult->GetNextElement()) {
            $applicationFields = $application->GetFields();
            $applicationProperties = $application->GetProperties();

            return [
                'SECTION_ID' => $applicationFields['IBLOCK_SECTION_ID'],
                'ALL_PROPERTIES' => $applicationProperties,
                'PROPERTIES' => [
                    'APPLICATION' => $applicationId,
                    'AMOUNT' => $applicationProperties['AMOUNT']['VALUE'],
                    'ELEMENT_PAY' => $applicationProperties['ELEMENT_PAY']['VALUE'],
                    'PACKAGE' => $applicationProperties['PACKAGE']['VALUE'],
                    'USER' => $applicationProperties['USER_MAIN']['VALUE'],
                ]
            ];
        }

        return false;
    }

    /**
     * Получение раздела оплаты по разделу заявки на оплату
     *
     * @param $applicationSectionId
     *
     * @return integer|boolean
     */
    public function getSectionIdByApplicationSection($applicationSectionId)
    {
        if (empty($applicationSectionId)) {
            return false;
        }

        $applicationResource = CIBlockSection::GetByID($applicationSectionId);
        if ($applicationSection = $applicationResource->GetNext()) {
            $sectionCode = $applicationSection['CODE'];
        } else {
            return false;
        }

        $sectionId = Iblock::getSectionIdByCode($sectionCode, $this->arParams['IBLOCK_PAYMENTS']);

        return empty($sectionId) ? false : $sectionId;
    }

    /**
     * Формирование данных для счета на оплату
     *
     * @param $resultId
     * @param $applicationData
     * @param $paymentData
     * @param $userType
     *
     * @return array
     */
    public function generateBillData($resultId, $applicationData, $paymentData, $userType)
    {
        $clientInfo = '';
        switch ($userType) {
            case 'entity':
                $clientInfo = $paymentData['LEGAL_COMPANY'] . ', ' . Loc::getMessage('PAYMENT_INN') . ' ' .
                    $paymentData['LEGAL_INN'] . ', ' . Loc::getMessage('PAYMENT_KPP') . ' ' .
                    $paymentData['LEGAL_KPP'] . ', ' . $paymentData['LEGAL_ADDRESS'];
                break;
            case 'individual':
                $clientInfo = Loc::getMessage('PAYMENT_INN') . ' ' . $paymentData['INDIVIDUAL_INN'] .
                    ', ' . $paymentData['INDIVIDUAL_ADDRESS'];
                break;
        }

        $elementName = Iblock::getElementNameById($applicationData['ELEMENT_PAY']['VALUE']);
        $packageName = Iblock::getElementNameById($applicationData['PACKAGE']['VALUE']);
        $countUsers = count($applicationData['USERS']['VALUE']);
        $priceAll = Text::numberToMoney($applicationData['AMOUNT']['VALUE']) . ' ' . Loc::getMessage('PAYMENT_RUB');

        return [
            'number' => (string)$resultId,
            'date' => $paymentData['ACTIVE_FROM'],
            'clientInfo' => $clientInfo,
            'products' => [
                'number' => '1',
                'name' => $elementName . ', ' . $packageName,
                'count' => (string)$countUsers,
                'units' => Loc::getMessage('PAYMENT_UNITS'),
                'price' => Text::numberToMoney($applicationData['AMOUNT']['VALUE'] / $countUsers) . ' ' . Loc::getMessage('PAYMENT_RUB'),
                'priceAll' => $priceAll,
            ],
            'payment' => [
                'price' => $priceAll,
                'priceVat' => '0',
                'priceAll' => $priceAll,
                'count' => '1',
                'priceString' => Text::numberToWords($applicationData['AMOUNT']['VALUE']),
            ]
        ];
    }

    /**
     * Возврат ошибки
     *
     * @param $text
     * @param $additional
     *
     * @return array
     */
    public static function returnError($text, $additional = null)
    {
        $error = new Error($text);
        $errorCollection = new ErrorCollection([$error]);
        return AjaxJson::createError($errorCollection, $additional);
    }

    /**
     * Отправка почтового шаблона новому пользователю
     *
     * @param $user
     * @param $elementId
     * @param $packageId
     *
     * @return void
     */
    public static function sendMessageToNewUser($user, $elementId, $packageId)
    {
        $elementName = Iblock::getElementNameById($elementId);
        $elementLink = Iblock::getElementDetailLinkById($elementId);
        $packageName = Iblock::getElementNameById($packageId);
        $emailFields = [
            'EMAIL' => $user['email'],
            'NAME' => $user['name'] . ' ' . $user['surname'],
            'ELEMENT_NAME' => $elementName,
            'ELEMENT_LINK' => Url::getFullUrl($elementLink),
            'PACKAGE_NAME' => $packageName,
            'DATE' => FormatDate('FULL', false, true),
            'USER_ID' => $user['ID'],
            'USER_PASSWORD' => $user['password'],
            'CONFIRM_CODE' => $user['confirmCode'],
            'SERVER_LINK' => Url::getFullUrl(),
        ];

        Event::send([
            'EVENT_NAME' => 'NEW_USER_APPLICATION',
            'LID' => SITE_ID,
            'C_FIELDS' => $emailFields,
        ]);
    }

    /**
     * Проверка и сохранение файла для пользователя
     *
     * @param $fileName
     * @param $fieldName
     * @param $userId
     * @param $userKey
     *
     * @return void
     */
    public static function checkAndSaveUserFile($fileName, $fieldName, $userId, $userKey)
    {
        if (empty($_FILES['users']['name'][$userKey][$fileName]) || empty($userId)) {
            return;
        }

        // Если у пользователе уже есть подтвержденный админом файл, то не пересохраняем его
        $userCheck = CUser::GetByID($userId)->Fetch();
        if (!empty($userCheck[$fieldName]) && $userCheck[$fieldName . '_CONFIRMED']) {
            return;
        }

        $userObject = new CUser;
        $fileData = [
            'name' => $_FILES['users']['name'][$userKey][$fileName],
            'type' => $_FILES['users']['type'][$userKey][$fileName],
            'tmp_name' => $_FILES['users']['tmp_name'][$userKey][$fileName],
            'size' => $_FILES['users']['size'][$userKey][$fileName],
        ];
        $userObject->Update($userId, [$fieldName => $fileData]);
    }
}
